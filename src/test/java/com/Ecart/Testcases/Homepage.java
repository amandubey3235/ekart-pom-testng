package com.Ecart.Testcases;


import com.automation.basemanage.Basemanagement;
import com.pageModel.Homepageobject;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class Homepage extends Basemanagement {
	
	Homepageobject homeobject;
	
	@BeforeMethod
	public void loadobjectclass() {
	 homeobject=new Homepageobject(driver);
	 

	}
	
	@Test
	public void home() {
		Assert.assertEquals(homeobject.verifyhomepage(), "Home");
		homeobject.redirecttoLoginPage();
		
	}
	

}
