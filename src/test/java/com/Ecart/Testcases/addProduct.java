package com.Ecart.Testcases;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.automation.basemanage.Basemanagement;
import com.pageModel.Homepageobject;
import com.pageModel.LoginPageObject;
import com.pageModel.Product_object;

public class addProduct extends Basemanagement {

	Homepageobject hobject;
	LoginPageObject lobject;
	Product_object pobject;
	Loginpage login = new Loginpage();

	@BeforeTest
	public void preloadclass() {

		hobject = new Homepageobject(driver);
		lobject = new LoginPageObject(driver);
		pobject = new Product_object(driver);

		hobject.redirecttoLoginPage();
		lobject.loginexistingUser("subdemologin@gmail.com", "Ad199908");
		lobject.confirmlogin();
		implicitcall(5000);

	}

	@Test(priority = 1,enabled=true)
	public void addOneIteam() {

		pobject.Singleproduct("Sleeveless Dress");

	}

	@Test(priority = 2, enabled=false)
	public void addMultipleIteam() {
		implicitcall(1000);
		pobject.MultipleItem(additeams());

	}
	@Test(priority=3,enabled=false)
	public void searchproducut() throws InterruptedException {
		pobject.searchProduct("Jeans");
	}
	
	@Test(priority=4,enabled=false)
	public void Catrgoryproducut()  {
		implicitcall(1000);
		pobject.categorySelection("MEN","Jeans");
	}
	
	@Test(priority=4)
	public void addtocart() {
		pobject.proceedcart();
		pobject.paymentMethod();
		pobject.verifyconfirmation();
	}

	private List<String> additeams() {
		List<String> items = new ArrayList<String>();
		items.add("Blue Top");
		items.add("Sleeveless Dress");

		return items;
	}

}
