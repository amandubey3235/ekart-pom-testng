package com.Ecart.Testcases;

import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.automation.basemanage.Basemanagement;
import com.ecart.utility.commonDatafiles;
import com.pageModel.Homepageobject;
import com.pageModel.LoginPageObject;

public class SIgnupUser extends Basemanagement {

	Homepageobject home;
	LoginPageObject loginp;
	commonDatafiles commondata = new commonDatafiles();

	@BeforeMethod
	public void loadpages() {
		home = new Homepageobject(driver);
		loginp = new LoginPageObject(driver);

	}

	@Test(priority = 1)
	public void userloginPage() {
		home.redirecttoLoginPage();

	}

	@Test(priority = 2)
	public void verifySignup() {

		Assert.assertEquals(loginp.signuppageVerifaction(), "New User Signup!");

	}

	@Test(priority = 3, dataProvider = "signupLogin")
	public void signupUser(String usrName, String usrEmail) {

		loginp.signupUser(usrName, usrEmail);
		loginp.confirmsignup();
		Map<String, String> userData = commonDatafiles.getUserData();
		loginp.filluserform(userData);
		loginp.verifycreatedAccount();
		implicitcall(10);
	}

	@Test(priority = 4)
	public void logoutUserEkart() throws InterruptedException {

		explicitWait(loginp.signupContinue());
		driver.navigate().refresh();
		loginp.logoutAccount();

	}

	@DataProvider(name = "signupLogin")
	public Object[][] signupusers() {

		return new Object[][] { { "subdemo", "subdemolog" + Math.random() + "@gmail.com"

				} };

	}
}
