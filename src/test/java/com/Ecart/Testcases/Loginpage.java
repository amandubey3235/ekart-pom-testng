package com.Ecart.Testcases;



import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import org.testng.annotations.Test;

import com.automation.basemanage.Basemanagement;
import com.ecart.utility.commonDatafiles;
import com.pageModel.Homepageobject;
import com.pageModel.LoginPageObject;

public class Loginpage extends Basemanagement {

	Homepageobject home;
	LoginPageObject loginp;
	commonDatafiles commondata = new commonDatafiles();

	@BeforeMethod
	public void loadpages() {
		home = new Homepageobject(driver);
		loginp = new LoginPageObject(driver);

	}

	@Test(priority = 1)
	public void userloginPage() {
		home.redirecttoLoginPage();

	}

	@Test(priority = 2)
	public void verifyloginpage() {

		Assert.assertEquals(loginp.loginpageVerifaction(), "Login to your accountt");
	}

	@Test(priority = 3, dataProvider = "Loginuser")
	public void logedinuser(String email, String passcode) {

		loginp.loginexistingUser(email, passcode);
		loginp.confirmlogin();

	}
	

	@DataProvider(name = "Loginuser")
	public Object[][] loggedUser() {

		return new Object[][] { { "subdemologin@gmail.com", "Ad19990"

				} };

	}

}
