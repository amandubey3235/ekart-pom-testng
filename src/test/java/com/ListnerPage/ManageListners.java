package com.ListnerPage;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.automation.basemanage.Basemanagement;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.reportmanage.CaptureScreenShot;
import com.reportmanage.ExtentReport;

public class ManageListners extends Basemanagement implements ITestListener {

	ExtentReports extent = ExtentReport.getReportObject();
	ExtentTest test;
	ThreadLocal<ExtentTest> extentTest=new ThreadLocal<ExtentTest>();
	
	private WebDriver driver;

	@Override
	public void onFinish(ITestContext Result) {
		extent.flush();

	}

	@Override
	public void onStart(ITestContext Result) {

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult Result) {

	}
	// When Test case get failed, this method is called.
	@Override
	public void onTestFailure(ITestResult Result) {
		test.log(Status.FAIL, "Test Failed");
		extentTest.get().fail(Result.getThrowable());
		try {
		driver= (WebDriver) Result.getTestClass().getRealClass().getField("driver").get(Result.getInstance());
		}
		catch(Exception e1) {
			e1.printStackTrace();
		}
		String filePath = null;
		try { 
			filePath = CaptureScreenShot.getScreenshot(Result.getMethod().getMethodName(), driver);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		test.addScreenCaptureFromPath(filePath, Result.getMethod().getMethodName());

	}

	// When Test case get Skipped, this method is called.
	@Override
	public void onTestSkipped(ITestResult Result) {
		System.out.println("The name of the testcase Skipped is :" + Result.getName());
	}

	// When Test case get Started, this method is called.
	@Override
	public void onTestStart(ITestResult Result) {
		extentTest.set(test);
		test = extent.createTest(Result.getMethod().getMethodName());
		System.out.println(Result.getName() + " test case started");
	}

	// When Test case get passed, this method is called.
	@Override
	public void onTestSuccess(ITestResult Result) {
		test.log(Status.PASS, "Test Passed");
		System.out.println("The name of the testcase passed is :" + Result.getName());
	}


}
