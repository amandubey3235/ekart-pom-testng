package com.automation.basemanage;

import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.Ecart.Testcases.Homepage;
import com.ListnerPage.IretryAnlizer;

public class Basemanagement {

	public  WebDriver driver;
	public Properties prop;

	public WebDriver lounchBrowser() throws IOException {

		prop = loadConfig();
		String browserName = prop.getProperty("browser");

		if (browserName.equalsIgnoreCase("chrome")) {

			// System.setProperty("webdriver.chrome.driver",
			// "C:\\Users\\amand\\OneDrive\\Documents\\manageDriver\\edgedriver_win64\\msedgedriver");
			driver = new ChromeDriver();

		} else if (browserName.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.chrome.driver",
					"C:\\Users\\amand\\OneDrive\\Documents\\manageDriver\\edgedriver_win64\\msedgedriver");
			driver = new FirefoxDriver();

		} else if (browserName.equalsIgnoreCase("edge")) {
			System.setProperty("webdriver.edge.driver",
					System.getProperty("user.dir") + "/src/main/java/com/manage/resource/msedgedriver.exe");
			driver = new EdgeDriver();

		} else {
			Assert.fail("Browser intialization failed either null or browser name not correctly setup" + browserName);
		}

		return driver;

	}

	public Properties loadConfig() throws IOException {
		Properties prop = new Properties();
		FileInputStream fis = new FileInputStream(
				System.getProperty("user.dir") + "/src/main/java/com/manage/resource/datahandle.properties");

		prop.load(fis);
		return prop;
	}

	public void implicitcall(int time) {

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
	}

	public void explicitWait(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
		element.click();
	}

	Logger log = LoggerFactory.getLogger(Basemanagement.class);

	@BeforeTest
	public void intialize() throws IOException {

		driver=lounchBrowser();
		log.info("Driver Initilized");
		driver.get(prop.getProperty("url"));
		driver.manage().window().maximize();

	}
	
	@BeforeSuite
    public void applyRetryAnalyzer(ITestContext context) {
		 List<ITestNGMethod> methods = Arrays.asList(context.getAllTestMethods());
		 methods.stream()
            .forEach(method -> method.setRetryAnalyzerClass(IretryAnlizer.class));
    }


	@AfterTest(enabled = true)
	public void teardown() {

		driver.close();

	}

}
