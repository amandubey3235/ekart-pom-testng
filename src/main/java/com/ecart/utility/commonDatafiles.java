package com.ecart.utility;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;

public class commonDatafiles {
	
	

	public static Map<String, String> getUserData() {
		String getdata = System.getProperty("user.dir") + "/src/main/java/com/manage/resource/datafill.json";
        Map<String, String> userData = new HashMap<>();

        try {
            FileReader reader = new FileReader(getdata);
            int data;
            StringBuilder jsonString = new StringBuilder();

            while ((data = reader.read()) != -1) {
                jsonString.append((char) data);
            }

            JSONObject jsonObject = new JSONObject(jsonString.toString());
            JSONArray usersArray = jsonObject.getJSONArray("users");

            if (usersArray.length() > 0) {
                JSONObject userObject = usersArray.getJSONObject(0);
                userData.put("name", userObject.getString("name"));
                userData.put("passcode", userObject.getString("passcode"));
                userData.put("lname", userObject.getString("lname"));
                userData.put("address", userObject.getString("address"));
                userData.put("city", userObject.getString("city"));
                userData.put("state", userObject.getString("state"));
                userData.put("zipcode", userObject.getString("zipcode"));
                userData.put("countryName", userObject.getString("countryName"));
                userData.put("phn", userObject.getString("phn"));
            } else {
                System.err.println("No user data found in the JSON file.");
            }
              
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return userData;
    }
}
