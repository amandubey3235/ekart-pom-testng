package com.ecart.utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Readcsvfile {
	
	
	public static List<HashMap<String, String>> readalldata() throws IOException {

		File file = new File("C:\\Users\\amand\\eclipse-workspace\\RestAssured_Hybrid\\testData\\UserBook.xlsx");

		FileInputStream fis = new FileInputStream(file);

		XSSFWorkbook workbook = new XSSFWorkbook(fis);

		XSSFSheet sheet = workbook.getSheetAt(0);

		List<HashMap<String, String>> storedata = new ArrayList<>();

		for (int rowIndex = 1; rowIndex <= sheet.getLastRowNum(); rowIndex++) {

			HashMap<String, String> rowData = new HashMap<>();
			Row row = sheet.getRow(rowIndex);

			String userId = getStringValue(row.getCell(0));
			String userName = getStringValue(row.getCell(1));
			String firstName = getStringValue(row.getCell(2));
			String lastName = getStringValue(row.getCell(3));
			String email = getStringValue(row.getCell(4));
			String password = getStringValue(row.getCell(5));
			String phone = getStringValue(row.getCell(6));
			
			rowData.put("UserID", userId);
			rowData.put("UserName", userName);
			rowData.put("FirstName", firstName);
			rowData.put("LastName", lastName);
			rowData.put("Email", email);
			rowData.put("Password", password);
			rowData.put("Phone", phone);
			
			storedata.add(rowData);
			
		}

		workbook.close();
		fis.close();

		return storedata;

	}

	private static String getStringValue(Cell cell) {

		switch (cell.getCellType()) {
		case STRING:
			return cell.getStringCellValue();
		case NUMERIC: {
			// If it's a decimal, keep it as is
			return String.valueOf((int) cell.getNumericCellValue());
		}
		default:
			return ""; // Handle other cell types as needed
		}
	}
	
	

}
