package com.pageModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Homepageobject {
	
	public WebDriver driver;
	
	 public Homepageobject(WebDriver driver) {
		
		 this.driver=driver;
		 PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//*[contains(text(),\"Home\")] ")
	WebElement homepagelogo;
	
	@FindBy(css="i.fa.fa-home")
	WebElement colorname;
	
	@FindBy(xpath="//a[@href=\"/login\"]")
	WebElement loginlink;
	
	
	public String verifyhomepage() {
		 
		return  homepagelogo.getText();
		
	}
	
	public void redirecttoLoginPage() {
		
		loginlink.click();
		
	}
	public String colorcode() {
		
		String rgb=colorname.getCssValue("color");;
		return rgb;
	}
	
	
	

}
