package com.pageModel;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class LoginPageObject {

	WebDriver driver;

	public LoginPageObject(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(css = "[class=\"radio-inline\"]")
	static List<WebElement> Getgender;

	@FindBy(xpath = "//*[@id=\"days\"]")
	static WebElement dayofdob;

	@FindBy(xpath = "//*[@id=\"months\"]")
	static WebElement monthofdob;

	@FindBy(xpath = "//*[@id=\"years\"]")
	static WebElement yearofdob;

	@FindBy(css = "[id=\"address1\"]")
	WebElement address1;

	@FindBy(css = "[id=\"city\"]")
	WebElement citycheck;

	@FindBy(css = "[id=\"zipcode\"]")
	WebElement pincode;

	@FindBy(css = "[id=\"mobile_number\"]")
	WebElement phnnumber;

	@FindBy(css = "[id=\"state\"]")
	WebElement statename;

	@FindBy(css = "[id=\"country\"]")
	WebElement country;

	@FindBy(css = "[data-qa=\"create-account\"]")
	WebElement createaccount;

	@FindBy(xpath = "//*[@id=\"form\"]/div/div/div/div/a")
	WebElement cntBtn;

	@FindBy(xpath = "//*[text()=\"Account Created!\"]")
	WebElement verifyacountcreated;

	@FindBy(css = "[name=\"optin\"]")
	WebElement checkboxoffer;

	@FindBy(xpath = "//*[contains(h2,'Login to your account')]/h2")
	WebElement loginpageverification;

	@FindBy(xpath = "//*[contains(h2,'New User Signup!')]/h2")
	WebElement signuppageverfication;

	@FindBy(xpath = "//*[contains(@name,'email') and @data-qa=\"login-email\"] ")
	WebElement emaillogin;

	@FindBy(xpath = "//*[contains(@name,'password') and @data-qa=\"login-password\"] ")
	WebElement passwordlogin;

	@FindBy(xpath = "//input[@id=\"password\"]")
	WebElement signuppasscode;

	@FindBy(xpath = "//*[@data-qa=\"login-button\"] ")
	WebElement login;

	@FindBy(xpath = "//*[@name=\"name\"] ")
	WebElement nameuser;

	@FindBy(css = "[name=\"first_name\"]")
	WebElement fnameuser;

	@FindBy(css = "[name=\"last_name\"]")
	WebElement lastnameuser;

	@FindBy(xpath = "//*[contains(@name,'email') and @data-qa=\"signup-email\"] ")
	WebElement emaillogup;

	@FindBy(xpath = "//*[@id=\"form\"]/div/div/div[3]/div/form/button")
	WebElement signup;

	@FindBy(xpath = "//*[text()=\"Enter Account Information\"]")
	WebElement signupformverfication;

	@FindBy(xpath = "//*[text()=\" Delete Account\"] ")
	WebElement deleteaccount;

	@FindBy(xpath = "//*[text()=\"Account Deleted!\"]  ")
	WebElement verifydeleted;

	@FindBy(xpath = "//*[text()=\"Account Deleted!\"]  ")
	WebElement continueafterdelete;

	@FindBy(xpath = "//*[@id=\"header\"]/div/div/div/div[2]/div/ul/li[4]/a")
	WebElement logouttheUser;

	public String loginpageVerifaction() {

		return loginpageverification.getText();
	}

	public String signuppageVerifaction() {

		return signuppageverfication.getText();
	}

	public void signupUser(String name, String email) {

		nameuser.sendKeys(name);
		emaillogup.sendKeys(email);

	}

	public void confirmsignup() {

		signup.click();

	}

	public void loginexistingUser(String email, String passcode) {

		emaillogin.sendKeys(email);
		passwordlogin.sendKeys(passcode);
	}

	public void confirmlogin() {

		login.click();

	}

	public String verfysignupformpage() {

		return signupformverfication.getText();

	}

	public static void getGenderOfUser() {

		for (WebElement genderoptions : Getgender) {

			String gender = genderoptions.getText().trim();

			if (gender.equalsIgnoreCase("Mr.")) {

				if (!genderoptions.isSelected()) {
					genderoptions.click();
				}
				break;
			}

		}

	}

	public static void selectdob() {

		Select dayselect = new Select(dayofdob);

		dayselect.selectByVisibleText("1");

		Select monthselect = new Select(monthofdob);

		monthselect.selectByVisibleText("March");

		Select yearselect = new Select(yearofdob);

		yearselect.selectByVisibleText("1998");

	}

	private void sendKeysToElement(WebElement element, String value) {
		if (value != null) {
			element.sendKeys(value);
		}
	}

	public void filluserform(Map<String, String> userdata) {

		getGenderOfUser();
		sendKeysToElement(signuppasscode, userdata.get("passcode"));
		selectdob();
		checkboxoffer.click();

		// Address Information

		fnameuser.sendKeys(userdata.get("name"));
		lastnameuser.sendKeys(userdata.get("lname"));
		address1.sendKeys(userdata.get("address"));
		Select slccountry = new Select(country);
		slccountry.selectByVisibleText(userdata.get("countryName"));
		statename.sendKeys(userdata.get("state"));
		citycheck.sendKeys(userdata.get("city"));
		pincode.sendKeys(userdata.get("zipcode"));
		phnnumber.sendKeys(userdata.get("phn"));
		createaccount.click();

	}

	public void verifycreatedAccount() {

		String msg = verifyacountcreated.getText();
		Assert.assertEquals(msg, "ACCOUNT CREATED!");

	}

	public WebElement signupContinue() {
		return cntBtn;
	}

	public void logoutAccount() throws InterruptedException {

		logouttheUser.click();
	}

	public void verifydeletedAccount() {

		String msg = verifydeleted.getText();
		Assert.assertEquals(msg, "Account Created!");
		continueafterdelete.click();

	}

}
