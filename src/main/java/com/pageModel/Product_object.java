package com.pageModel;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import ch.qos.logback.core.recovery.ResilientSyslogOutputStream;

public class Product_object {

	WebDriver driver;

	public Product_object(WebDriver driver) {

		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(xpath = "//*[text()=\" Products\"]")
	WebElement productCat;

	// search dynamic project

	@FindBy(xpath = "//*[@class='productinfo text-center']/p")
	List<WebElement> productname;

	@FindBy(xpath = "//*[text()=\"Continue Shopping\"]")
	WebElement continueShoping;

	// category add product

	@FindBy(xpath = "//*[@class='panel-title']/a")
	List<WebElement> expandcategory;

	// Search add Product

	@FindBy(xpath = "//*[@id='search_product']")
	WebElement typesearchProduct;

	@FindBy(xpath = "//*[@id='submit_search']")
	WebElement searchProduct;

	// Cart Page

	@FindBy(xpath = "//*[text()=' Cart']")
	WebElement cartsection;

	@FindBy(xpath = "//*[text()='Proceed To Checkout']")
	WebElement prceedcheckout;

	@FindBy(xpath = "//*[@colspan='2']//following-sibling::td/p[@class='cart_total_price']")
	WebElement totalamount;

	@FindBy(xpath = "//*[@class='cart_price']")
	List<WebElement> Itemamounts;

	@FindBy(xpath = "//*[@colspan='2']/h4/b")
	WebElement totalamounttext;

	@FindBy(xpath = "//*[text()='Place Order']")
	WebElement placeodr;

	// Payment Section

	@FindBy(xpath = "//*[@class='heading']")
	WebElement paymentText;

	@FindBy(xpath = "//*[@name='name_on_card']")
	WebElement cardname;

	@FindBy(xpath = "//*[@name='card_number']")
	WebElement cardnumber;

	@FindBy(xpath = "//*[@name='cvc']")
	WebElement cardcvv;

	@FindBy(xpath = "//*[@name='expiry_month']")
	WebElement expiryMonth;

	@FindBy(xpath = "//*[@name='expiry_year']")
	WebElement expiryYear;

	@FindBy(xpath = "//*[@id='submit']")
	WebElement payConfirm;

	@FindBy(xpath = "//*[@class=\"col-sm-9 col-sm-offset-1\"]/p")
	WebElement confirmationMessage;

	@FindBy(xpath = "//*[text()='Continue']")
	WebElement continuetohome;

	int btnclick;

	public void manageaddcart() {
		driver.findElement(By.xpath(" //*[@data-product-id='" + btnclick + "']")).click();
		driver.findElement(By.xpath(" //*[@data-product-id='" + btnclick + "']")).click();
	}

	public void Singleproduct(String prdname) {

		productCat.click();

		List<String> cartitems = new ArrayList<String>();
		for (int i = 0; i < productname.size(); i++) {
			cartitems.add(productname.get(i).getText());
		}
		for (int i = 0; i < cartitems.size(); i++) {
			if (cartitems.get(i).equalsIgnoreCase(prdname)) {

				btnclick = i + 1;
				manageaddcart();
				explicitProductWait(continueShoping);
			}
		}

	}

	public void MultipleItem(List<String> listofproduct) {

		for (int i = 0; i < productname.size(); i++) {
			String productNameText = productname.get(i).getText();

			if (listofproduct.contains(productNameText)) {

				btnclick = i + 1;
				manageaddcart();
				explicitProductWait(continueShoping);
				implicitcallPages(3000);
			}
		}

	}

	public void categorySelection(String gender, String clothType) {

		productCat.click();

		for (int i = 0; i < expandcategory.size(); i++) {
			if (expandcategory.get(i).getText().equalsIgnoreCase(gender)) {

				expandcategory.get(i).click();
				break;
			}
		}
		if (gender.equalsIgnoreCase(clothType)) {
			// Men //Kids
			List<WebElement> typesOfDress = driver.findElements(By.xpath("//*[@id='" + clothType + "']/div/ul/li/a"));

			for (int i = 0; i < typesOfDress.size(); i++) {

				if (typesOfDress.get(i).getText().equalsIgnoreCase(clothType)) {
					typesOfDress.get(i).click();
					break;
				}

			}

		} else {
			Assert.fail("Gender and cloth Type is miss-match pls verify again");
		}
		btnclick = 33;
		manageaddcart();
		explicitProductWait(continueShoping);

	}

	public void searchProduct(String clothType) throws InterruptedException {
		productCat.click();
		typesearchProduct.click();
		typesearchProduct.sendKeys(clothType);
		searchProduct.click();

		btnclick = 33;
		manageaddcart();
		explicitProductWait(continueShoping);

	}

	public void proceedcart() {
		
		cartsection.click();
		prceedcheckout.click();

		String countAmount;
		int sum = 0;
		if (Itemamounts.size() > 1) {

			for (int i = 0; i < Itemamounts.size(); i++) {
				countAmount = Itemamounts.get(i).getText();
				sum = sum + Integer.parseInt(countAmount);
			}

			String totalamt = totalamount.getText();
			System.out.println("Your Price" + sum + " total price " + totalamt);

		} else {

			if (Itemamounts.get(0).getText().equalsIgnoreCase(totalamount.getText())) {

				System.out.println("Item price and total amount matches which is: " + totalamounttext.getText() + " "
						+ totalamount.getText());
			} else {
				System.out.println("Price did not match");
			}
		}
		
		placeodr.click();

	}

	public void paymentMethod() {

		Assert.assertEquals(paymentText.getText(), paymentText.getText());

		cardname.sendKeys("HDFC");
		cardnumber.sendKeys("33445577899");
		cardcvv.sendKeys("333");
		expiryMonth.sendKeys("April");
		expiryYear.sendKeys("2030");
		payConfirm.click();

	}

	public void verifyconfirmation() {

		Assert.assertEquals(confirmationMessage.getText(), confirmationMessage.getText());
		continuetohome.click();

	}

	public WebElement contniueBtn() {
		return continueShoping;
	}

	public void explicitProductWait(WebElement webElement) {
		WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));

		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(webElement));
		element.click();
	}

	public void implicitcallPages(int time) {

		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(time));
	}

}
